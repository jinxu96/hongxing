 //首页轮播图
 var swiper1 = new Swiper('.swiper1', {
        pagination: '.page1',
//      nextButton: '.swiper-button-next',
//      prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 0,
        centeredSlides: true,
        autoplay: 5000,
        autoplayDisableOnInteraction: false
    });
    

$(function(){
	//首页产品切换
	$(".products ul.pro_list li").eq(0).show();
    $(".products ul.pro_tt li").eq(0).addClass("hover");
    $(".products ul.pro_tt li").hover(function(){
    var num =$(".products ul.pro_tt li").index(this);
    $(".products ul.pro_tt li").removeClass("hover");
    $(this).addClass("hover");
    $(".products ul.pro_list li").hide();
    $(".products ul.pro_list li").eq(num).show().slblings().hide();
	});
	

	//侧栏吸附
	
	window.onscroll = function() {
	var ht=$(".yijia").offset().top;
	var ft=$(".footer").offset().top-$('.yj_left').height()+50;
	if($(document).scrollTop() > ht && $(document).scrollTop() < ft) {
	$('.yj_left').addClass('fixed');
	} else {
	$('.yj_left').removeClass('fixed');
	}	
	if($(document).scrollTop() > ft){
	$('.yj_left').addClass('absolute');	
	}else{
	$('.yj_left').removeClass('absolute');	
	}
	}

	//表单验证
    $(".submit").click(function(){
			var retel = /^(13\d|14\d|15\d|17\d|18\d|166|199)[0-9]{8}$/;
			var rewei = /^[a-zA-Z\d_]{5,}$/;
			if ($("#user").val()== "") {
				alert("您的姓名不能为空!");
				return false;
			}else if ($("#address").val()=="") {
				alert("请您填写联系地址");
				return false;
			}else if ($("#tel").val()=="") {
				alert("请您填写手机号码!");
				return false;
			}else if (retel.test($("#tel").val()) == false) {
				alert("手机号码格式不正确!");
				return false;
			} else {
				//alert("预约成功!");
			    return true;
			}
	});

//产品切换

				$(".hglist").scrollable({size:5,items:".hglist",speed:500,loop:false}).autoscroll({autoplay:false,interval:5000,steps:1}).navigator({});
			
		       $(".home_01 .d1 .scroll").scrollable({size:3,items:".home_01 .d1 .scroll",speed:500,loop:true}).autoscroll({autoplay:true,interval:5000,steps:3}).navigator({navi:".home_01_u1",naviItem:"li",activeClass:"focus"});               
		        $(".mc_1 .mc_1_d .scroll").scrollable({size:1,items:".mc_1 .mc_1_d",speed:500,loop:true}).autoscroll({autoplay:true,interval:5000,steps:4}).navigator({navi:".home_01_u1",naviItem:"li",activeClass:"focus"});


//
$(document).ready(function(){                          
    var index=0;
    var length=$("#img img").length;
    var i=1;
    
    //关键函数：通过控制i ，来显示图片
    function showImg(i){
        $("#img img")
            .eq(i).stop(true,true).fadeIn(800)
            .siblings("img").hide();
         $("#cbtn li")
            .eq(i).addClass("hov")
            .siblings().removeClass("hov");
    }
    
    function slideNext(){
        if(index >= 0 && index < length-1) {
             ++index;
             showImg(index);
        }else{
//			if(confirm("已经是最后一张,点击确定重新浏览！")){
//				showImg(0);
//				index=0;
//				aniPx=(length-5)*72+'px'; //所有图片数 - 可见图片数 * 每张的距离 = 最后一张滚动到第一张的距离
//				$("#cSlideUl ul").animate({ "left": "+="+aniPx },200);
//				i=1;
//			}
            return false;
        }
        if(i<0 || i>length-5) {return false;}						  
               $("#cSlideUl ul").animate({ "left": "-=70px" },200)
               i++;
    }
     
    function slideFront(){
       if(index >= 1 ) {
             --index;
             showImg(index);
        }
        if(i<2 || i>length+5) {return false;}
               $("#cSlideUl ul").animate({ "left": "+=70px" },200)
               i--;
    }	
        $("#img img").eq(0).show();
        $("#cbtn li").eq(0).addClass("hov");
        $("#cbtn tt").each(function(e){
            var str=(e+1)+"/"+length;
            $(this).html(str)
        })
    
        $(".picSildeRight,#next").click(function(){
               slideNext();
           })
        $(".picSildeLeft,#front").click(function(){
               slideFront();
           })
        $("#cbtn li").click(function(){
            index  =  $("#cbtn li").index(this);
            showImg(index);
        });	
		$("#next,#front").hover(function(){
			$(this).children("a").fadeIn();
		},function(){
			$(this).children("a").fadeOut();
		})
    })	



})
