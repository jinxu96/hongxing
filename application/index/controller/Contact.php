<?php

namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Contact extends Base
{
	//联系我们
	public function contact(){
		//导航栏栏目
    	$column = Db::table('column')->select();
    	//Banner图
		$banner = Db::table('banner')->where('id=13')->find();
		//友情链接
		$link = Db::table('link')->select();


    	$this->assign('column',$column);
		$this->assign('banner',$banner);
		$this->assign('link',$link);
		return $this->fetch();
	}

	public function add(){
		//判断页面是否提交
		// print_r($_POST);exit();
		if(request()->isPost()){
			//接收传递过来的值
			//input都是封装好了，相当于POST['xxx'];
			$data = [
				'name' => input('name'),
				'address' => input('address'),
				'phone' => input('phone'),
				'content' => input('content'),
				'time' => time()
			];
			// dump($data);exit;
			if(Db::table('message')->insert($data)){  //添加数据
				return "<script>alert('留言成功');window.location.href='contact';</script>";
			}else{
				return "<script>window.location.href='contact';</script>";
			}
			return;
			
		}
		return $this->fetch('contact');
	}
}