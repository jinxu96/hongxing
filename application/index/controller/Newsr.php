<?php

namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Newsr extends Base
{
	//新闻中心
	public function news(){
		//导航栏栏目
    	$column = Db::table('column')->select();
    	//Banner图
    	$banner = Db::table('banner')->where('id=12')->find();
    	//新闻中心
    	$news = Db::table('newsr')->order('time desc')->paginate(12);
        //友情链接
        $link = Db::table('link')->select();

    	$this->assign('column',$column);
    	$this->assign('banner',$banner);
        $this->assign('news',$news);
        $this->assign('link',$link);
    	return $this->fetch();
	}
	//案例详情
    public function detail(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=12')->find();
        //获得数据
        $detail = Db::table('newsr')->where('id',input('id'))->find();
        //取出上一篇和下一篇的id
        $prev = Db::table('newsr')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('newsr')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('newsr')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('newsr')->where('id','>',input('id'))->order('id asc')->field('title')->find();
        //友情链接
        $link = Db::table('link')->select();

        $this->assign('detail',$detail);
        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);
        $this->assign('link',$link);

        return $this->fetch('newsdetail');
    }
}