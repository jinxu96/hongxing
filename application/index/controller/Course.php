<?php

namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Course extends Base
{
	//关于我们
	public function about(){
		//导航栏栏目
    	$column = Db::table('column')->select();
    	//Banner图
    	$banner = Db::table('banner')->where('id=5')->find();
    	//公司历程
    	$course = Db::table('course')->select();
    	//团队展示
    	$team = Db::table('team')->select();
    	//企业荣誉
		$honor = Db::table('honor')->select();
        //友情链接
        $link = Db::table('link')->select();

    	$this->assign('column',$column);
    	$this->assign('banner',$banner);
    	$this->assign('course',$course);
    	$this->assign('team',$team);
		$this->assign('honor',$honor);
		$this->assign('link',$link);
    	return $this->fetch();
	}
}