<?php

namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Search extends Base
{
	public function index(){
		//导航栏栏目
		$column = Db::table('column')->select();
		//友情链接
		$link = Db::table('link')->select();
		$keywords = input('keywords');
		if($keywords){
			$map['title']=['like','%'.$keywords.'%'];
			$searchres = Db::table('news_xq')->where($map)->order('id desc')->paginate(3,true);
			$tep = json_encode($searchres);
			$ret = (array)json_decode($tep);
			if (count($ret['data']) != 0) {
				$this->assign(array(
				'searchres' => $searchres,
				'keywords' => $keywords
			));
			}
			else {
				$this->assign(array(
				'searchres' => [['title'=>'暂无数据......','content'=>null]],
				'keywords' => null
			));
			}
		}
		$this->assign('column',$column);
		$this->assign('link',$link);
		return $this->fetch('search');
	}
}