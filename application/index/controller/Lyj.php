<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Lyj extends Base
{	
	//首页
	public function index(){
		//导航栏栏目
    	$column = Db::table('column')->select();
    	//Banner图
    	$banner = Db::table('banner')->where('id=6')->find();
    	//获得一级表数据
    	$res = Db::table('category')->select();
        $cate = Db::table('cate')->where('pid',input('pid'))->select();
        $cate1 = Db::table('cate')->where('pid=1')->select();
        //友情链接
        $link = Db::table('link')->select();
        
    	$this->assign('column',$column);
    	$this->assign('banner',$banner);
    	$this->assign('res',$res);
    	$this->assign('cate',$cate);
    	$this->assign('cate1',$cate1);
        $this->assign('link',$link);
		return $this->fetch('hanger');
	}
	//详情页
    public function detail(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=6')->find();
        //查询二级表的数据
        $data = Db::table('cate')->where('id',input('id'))->select();
        // dump($data);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('cate')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('cate')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('cate')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('cate')->where('id','>',input('id'))->order('id asc')->field('title')->find();
        //友情链接
        $link = Db::table('link')->select();
        
        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);
        $this->assign('link',$link);

        return $this->fetch('hangerdetail');
    }
    //关于我们
    public function about(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=18')->find();
        //历程
        $course_1 = Db::table('course_1')->select();
        //人才展示
        $rencai = Db::table('rencai_show')->select();
        //关于我们红杏优势
        $youshi = Db::table('aboutys_lyj')->select();
        //荣誉
        $honor_lyj = Db::table('honor_lyj')->select();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('course_1',$course_1);
        $this->assign('rencai',$rencai);
        $this->assign('youshi',$youshi);
        $this->assign('honor_lyj',$honor_lyj);

        return $this->fetch();
    }
    //关于我们红杏优势详情
    public function detail_youshi(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=18')->find();
        //获得数据
        $detail = Db::table('aboutysxq_lyj')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('aboutysxq_lyj')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('aboutysxq_lyj')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('aboutysxq_lyj')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('aboutysxq_lyj')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('aboutysdetail');
    }
    //工程案例
    public function project(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=19')->find();
        //工程案例
        $gongc = Db::table('gongclyj')->paginate(12);

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('gongc',$gongc);

        return $this->fetch('case');
    }
    //案例详情
    public function detail_gongc(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=19')->find();
        //获得数据
        $detail = Db::table('gongclyj_xq')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('gongclyj_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('gongclyj_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('gongclyj')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('gongclyj')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('casedetail');
    }
    //招商加盟
    public function zhaoshang(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=14')->find();
        //加盟案例数据
        $data = Db::table('jiameng')->select();
        //加盟要求
        $jiameng = Db::table('jiameng_yq')->select();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);
        $this->assign('jiameng',$jiameng);


        return $this->fetch('league');
    }
    //案例详情
    public function detail_jiameng(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=14')->find();
        //获得数据
        $detail = Db::table('jiameng_xq')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('jiameng_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('jiameng_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('jiameng')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('jiameng')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('leaguedetail');
    }
    //招商加盟留言
    public function add_jm(){
        //判断页面是否提交
        // print_r($_POST);exit();
        if(request()->isPost()){
            //接收传递过来的值
            //input都是封装好了，相当于POST['xxx'];
            $data = [
                'name' => input('name'),
                'address' => input('address'),
                'phone' => input('phone'),
                'content' => input('content'),
                'time' => time()
            ];
            // dump($data);exit;
            if(Db::table('message_jm')->insert($data)){  //添加数据
                return "<script>alert('留言成功');window.location.href='zhaoshang';</script>";
            }else{
                return "<script>window.location.href='zhaoshang';</script>";
            }
            return;
            
        }
        return $this->fetch('league');
    }
    //新闻中心页面
    public function news(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=20')->find();
        //获得左边新闻中心数据
        $data = Db::table('newsl_lyj')->select();
        //获得右边新闻中心数据
        $data2 = Db::table('newsrr_lyj')->select();
        // dump($data);exit;
        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);
        $this->assign('data2',$data2);

        return $this->fetch();
    }
    //左边新闻详情页
    public function detail_newsl(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=20')->find();
        //获得左边新闻详情数据
        $detail = Db::table('newsllyj_xq')->where('id',input('id'))->find();
        //取出上一篇和下一篇的id
        $prev = Db::table('newsllyj_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('newsllyj_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('newsl_lyj')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('newsl_lyj')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('newsldetail');
    }
    //右边新闻详情页
    public function detail_newsr(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=20')->find();
        //获得左边新闻详情数据
        $detail = Db::table('newsrrlyj_xq')->where('id',input('id'))->find();
        //取出上一篇和下一篇的id
        $prev = Db::table('newsrrlyj_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('newsrrlyj_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('newsrr_lyj')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('newsrr_lyj')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('newsrdetail');
    }
    //联系我们
    public function contact(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=21')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);

        return $this->fetch();
    }
}