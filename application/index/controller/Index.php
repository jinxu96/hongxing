<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Index extends Base
{	
	//首页
    public function index()
    {	
    	//导航栏栏目
    	$column = Db::table('column')->select();
        // dump($column);exit();
    	//Banner图
    	$banner = Db::table('banner_sy')->select();
    	//产品展示
    	$product_name = Db::table('product')->field('name')->select();
    	$product = Db::table('product')->select();
    	//加入红杏
    	$joinhx1 = Db::table('joinhx')->where('id<5')->select();
    	$joinhx2 = Db::table('joinhx')->where('id>4')->select();
    	//新闻中心
        $news = Db::table('newsr')->limit(4)->order('id desc')->select();
    	//加盟案例
    	$anli = Db::table('anli')->limit(6)->order('id desc')->select();
        //友情链接
        $link = Db::table('link')->select();

    	$this->assign('column',$column);
    	$this->assign('banner',$banner);
    	$this->assign('joinhx1',$joinhx1);
    	$this->assign('joinhx2',$joinhx2);
    	$this->assign('news',$news);
    	$this->assign('anli',$anli);
    	$this->assign('product_name',$product_name);
        $this->assign('product',$product);
        $this->assign('link',$link);
        return $this->fetch();
    }
    //招商加盟案例详情
    public function detail_anli(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=11')->find();
        //获得数据
        $detail = Db::table('anli')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('anli')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('anli')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('anli')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('anli')->where('id','>',input('id'))->order('id asc')->field('title')->find();
        //友情链接
        $link = Db::table('link')->select();

        $this->assign('detail',$detail);
        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);
        $this->assign('link',$link);

        return $this->fetch('anlidetail');
    }
}
