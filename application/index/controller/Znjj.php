<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Znjj extends Base
{
	public function index(){
        //导航栏栏目
        $column = Db::table('column')->select();
		//Banner图
        $banner = Db::table('banner')->where('id=9')->find();
        //获得一级表数据
        $res = Db::table('categoryznjj')->select();
        $cate = Db::table('cateznjj')->where('pid',input('pid'))->select();
        //默认页面
        $cate1 = Db::table('cateznjj')->where('pid=1')->select();
        //友情链接
        $link = Db::table('link')->select();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('res',$res);
        $this->assign('cate',$cate);
        $this->assign('cate1',$cate1);
        $this->assign('link',$link);

		return $this->fetch('smart');
	}
	//详情页
    public function detail(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=9')->find();
        //查询二级表的数据
        $data = Db::table('cateznjj')->where('id',input('id'))->select();
        // dump($data);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('cateznjj')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('cateznjj')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('cateznjj')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('cateznjj')->where('id','>',input('id'))->order('id asc')->field('title')->find();
        //友情链接
        $link = Db::table('link')->select();
        
        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);
        $this->assign('link',$link);


        return $this->fetch('smartdetail');
    }
    //关于我们页面
    public function about(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=30')->find();
        //人才展示
        $team = Db::table('team_znjj')->select();
        //大事记
        $res = Db::table('course_znjj')->select();
        //企业荣誉
        $honor = Db::table('honor_znjj')->select();
        //员工风采
        $data = Db::table('yuangfcai')->select();

        $this->assign('column',$column);
    	$this->assign('banner',$banner);
    	$this->assign('team',$team);
    	$this->assign('res',$res);
    	$this->assign('honor',$honor);
    	$this->assign('honor',$honor);
    	$this->assign('data',$data);

    	return $this->fetch();
    }
    //工程案例
    public function casea(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=31')->find();
        //工程案例第一个
        $gongc_1 = Db::table('gongc_znjj')->where('id=1')->find();
        //工程案例后面的数据
        $gongc = Db::table('gongc_znjj')->where('id>1')->paginate(9);

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('gongc_1',$gongc_1);
        $this->assign('gongc',$gongc);

        return $this->fetch('case');
    }
    //案例详情
    public function detail_gongc(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=31')->find();
        //获得数据
        $detail = Db::table('gongcznjj_xq')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('gongcznjj_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('gongcznjj_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('gongc_znjj')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('gongc_znjj')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('casedetail');
    }
    //招商加盟
    public function league(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=32')->find();
        //大众创业
        $data = Db::table('zhaosznjj')->select();
        //加盟案例展示
        $res = Db::table('jiameng_znjj')->select();
        //红杏品牌优势
        $pinpai = Db::table('zhaosys_znjj')->select();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);
        $this->assign('res',$res);
        $this->assign('pinpai',$pinpai);

    	return $this->fetch();
    }
    //加盟案例详情
    public function detail_league(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=32')->find();
        //获得数据
        $detail = Db::table('jiamengznjj_xq')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('jiamengznjj_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('jiamengznjj_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('jiamengznjj_xq')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('jiamengznjj_xq')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('leaguedetail');
    }
    //新闻中心案例
    public function news(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=33')->find();
        //新闻中心前两个案例
        $new = Db::table('news_znjj')->where('id<3')->select();
        //新闻中心案例后面的数据
        $news = Db::table('news_znjj')->where('id>2')->paginate(7);

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('new',$new);
        $this->assign('news',$news);

        return $this->fetch();
    }
    //案例详情
    public function detail_news(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=33')->find();
        //获得数据
        $detail = Db::table('newsznjj_xq')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('newsznjj_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('newsznjj_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('news_znjj')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('news_znjj')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('newsdetail');
    }
    //联系我们页面
    public function contact(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=34')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);

        return $this->fetch();
    }
    //联系我们留言
    public function add_contact(){
        //判断页面是否提交
        // print_r($_POST);exit();
        if(request()->isPost()){
            //接收传递过来的值
            //input都是封装好了，相当于POST['xxx'];
            $data = [
                'name' => input('name'),
                'phone' => input('phone'),
                'address' => input('address'),
                'content' => input('content'),
                'time' => time()
            ];
            // dump($data);exit;
            if(Db::table('message_znjj')->insert($data)){  //添加数据
                return "<script>alert('留言成功');window.location.href='contact';</script>";
            }else{
                return "<script>window.location.href='contact';</script>";
            }
            return;
            
        }
        return $this->fetch('contact');
    }
}