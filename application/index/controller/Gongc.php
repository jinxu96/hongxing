<?php

namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Gongc extends Base
{	
	//工程案例
	public function project(){
		//导航栏栏目
    	$column = Db::table('column')->select();
    	//Banner图
    	$banner = Db::table('banner')->where('id=10')->find();
    	//工程案例
    	$gongc = Db::table('gongc')->paginate(12);
        //友情链接
        $link = Db::table('link')->select();

    	$this->assign('column',$column);
    	$this->assign('banner',$banner);
        $this->assign('gongc',$gongc);
        $this->assign('link',$link);
		return $this->fetch();
	}
	//案例详情
    public function detail(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=10')->find();
        //获得数据
        $detail = Db::table('gongc')->where('id',input('id'))->find();
        //取出上一篇和下一篇的id
        $prev = Db::table('gongc')->where('id','<',input('id'))->order('id desc')->value('id');
        $next = Db::table('gongc')->where('id','>',input('id'))->order('id asc')->value('id');
        //友情链接
        $link = Db::table('link')->select();

        $name_prev = Db::table('gongc')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('gongc')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('detail',$detail);
        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);
        $this->assign('link',$link);

        return $this->fetch('casedetail');
    } 

}