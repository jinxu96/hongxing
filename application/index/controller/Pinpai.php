<?php

namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Pinpai extends Base
{	
	//招商加盟
	public function league(){
		//导航栏栏目
    	$column = Db::table('column')->select();
    	//Banner图
    	$banner = Db::table('banner')->where('id=11')->find();
    	//品牌实力
    	$pinpai = Db::table('pinpai')->select(); 
    	//成功案例
		$success = Db::table('gongc')->limit(6)->order('id desc')->select();
        //友情链接
        $link = Db::table('link')->select();

		$this->assign('column',$column);
    	$this->assign('banner',$banner);
    	$this->assign('pinpai',$pinpai);
		$this->assign('success',$success);
		$this->assign('link',$link);
		return $this->fetch();
	}
}