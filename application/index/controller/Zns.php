<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Zns extends Base
{
	public function index(){
        //导航栏栏目
        $column = Db::table('column')->select();
		//Banner图
        $banner = Db::table('banner')->where('id=8')->find();
        //获得一级表数据
        $res = Db::table('categoryzns')->select();
        $cate = Db::table('catezns')->where('pid',input('pid'))->select();
        //默认页面
        $cate1 = Db::table('catezns')->where('pid=1')->select();
        //友情链接
        $link = Db::table('link')->select();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('res',$res);
        $this->assign('cate',$cate);
        $this->assign('cate1',$cate1);
        $this->assign('link',$link);

		return $this->fetch('lock');
	}
	//详情页
    public function detail(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=8')->find();
        //查询二级表的数据
        $data = Db::table('catezns')->where('id',input('id'))->find();
        // dump($data);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('catezns')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('catezns')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('catezns')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('catezns')->where('id','>',input('id'))->order('id asc')->field('title')->find();
        //友情链接
        $link = Db::table('link')->select();
        
        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);
        $this->assign('link',$link);


        return $this->fetch('lockdetail');
    }
    //关于我们页面
    public function about(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=17')->find();
        //荣誉
        $honor = Db::table('honor_zns')->select();
        //我们的团队
        $team = Db::table('team_zns')->select();
        //获取客户案例
        $data = Db::table('anli_zns')->select();

        $this->assign('column',$column);
    	$this->assign('banner',$banner);
    	$this->assign('honor',$honor);
    	$this->assign('team',$team);
    	$this->assign('data',$data);

    	return $this->fetch();
    }
    //工程案例
    public function casea(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=26')->find();
        //工程案例
        $gongc = Db::table('gongc_zns')->paginate(8);

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('gongc',$gongc);

        return $this->fetch('case');
    }
    //案例详情
    public function detail_gongc(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=26')->find();
        //获得数据
        $detail = Db::table('gongczns_xq')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('gongczns_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('gongczns_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('gongc_zns')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('gongc_zns')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('casedetail');
    }
    //招商加盟
    public function league(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=27')->find();
        //加盟优势
        $data = Db::table('zhaosys_zns')->select();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);

    	return $this->fetch();
    }
    //招商加盟留言
    public function add_jm(){
        //判断页面是否提交
        // print_r($_POST);exit();
        if(request()->isPost()){
            //接收传递过来的值
            //input都是封装好了，相当于POST['xxx'];
            $data = [
                'name' => input('name'),
                'address' => input('address'),
                'phone' => input('phone'),
                'content' => input('content'),
                'time' => time()
            ];
            // dump($data);exit;
            if(Db::table('message_zns')->insert($data)){  //添加数据
                return "<script>alert('留言成功');window.location.href='league';</script>";
            }else{
                return "<script>window.location.href='league';</script>";
            }
            return;
            
        }
        return $this->fetch('league');
    }
    //新闻中心
    public function news(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=28')->find();
        //获取新闻列表
        $data = Db::table('news_zns')->paginate(9);

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);

    	return $this->fetch();
    }
    //新闻详情
    public function detail_news(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=28')->find();
        //获得数据
        $detail = Db::table('newszns_xq')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('newszns_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('newszns_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('news_zns')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('news_zns')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('newsdetail');
    }
    //联系我们页面
    public function contact(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=29')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);

        return $this->fetch();
    }
}