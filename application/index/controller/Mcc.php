<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Mcc extends Base
{
	//门窗页
    public function index(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=15')->find();
        //获得一级表数据
        $res = Db::table('categorymc')->select();
        $cate = Db::table('catemc')->where('pid',input('pid'))->select();
        //默认页面
        $cate1 = Db::table('catemc')->where('pid=1')->select();
        //友情链接
        $link = Db::table('link')->select();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('res',$res);
        $this->assign('cate',$cate);
        $this->assign('cate1',$cate1);
        $this->assign('link',$link);

        return $this->fetch('doors');
    }
    //详情页
    public function detail(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=15')->find();
        //查询二级表的数据
        $data = Db::table('catemc')->where('id',input('id'))->select();
        // dump($data);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('catemc')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('catemc')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('catemc')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('catemc')->where('id','>',input('id'))->order('id asc')->field('title')->find();
        //友情链接
        $link = Db::table('link')->select();
        
        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);
        $this->assign('link',$link);


        return $this->fetch('doorsdetail');
    }
    //关于我们
    public function about(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=16')->find();
        //中间展览图
        $data = Db::table('about_mc')->select();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);

        return $this->fetch();
    }
    //工程案例
    public function casea(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=22')->find();
        //工程案例
        $gongc = Db::table('gongc_mc')->paginate(8);

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('gongc',$gongc);

        return $this->fetch('case');
    }
    //案例详情
    public function detail_gongc(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=22')->find();
        //获得数据
        $detail = Db::table('gongcmc_xq')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('gongcmc_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('gongcmc_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('gongc_mc')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('gongc_mc')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('casedetail');
    }
    //招商加盟
    public function league(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=23')->find();
        //加盟要求
        $data = Db::table('jiamengmc_yq')->select();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);

    	return $this->fetch();
    }
    //招商加盟留言
    public function add_jm(){
        //判断页面是否提交
        // print_r($_POST);exit();
        if(request()->isPost()){
            //接收传递过来的值
            //input都是封装好了，相当于POST['xxx'];
            $data = [
                'name' => input('name'),
                'sex' => input('sex'),
                'phone' => input('phone'),
                'address' => input('address'),
                'time' => time()
            ];
            // dump($data);exit;
            if(Db::table('shenqjm')->insert($data)){  //添加数据
                return "<script>alert('申请成功');window.location.href='league';</script>";
            }else{
                return "<script>window.location.href='league';</script>";
            }
            return;
            
        }
        return $this->fetch('league');
    }
    //新闻中心
    public function news(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=24')->find();
        //获取新闻列表
        $data = Db::table('news_mc')->paginate(6);

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('data',$data);

    	return $this->fetch();
    }
    //新闻详情
    public function detail_news(){
        //导航栏栏目
        $column = Db::table('column')->select();
        //Banner图
        $banner = Db::table('banner')->where('id=24')->find();
        //获得数据
        $detail = Db::table('newsmc_xq')->where('id',input('id'))->find();
        // dump($detail);exit;
        //取出上一篇和下一篇的id
        $prev = Db::table('newsmc_xq')->where('id','<',input('id'))->order('id desc')->value('id');
        // dump($detail);exit;
        $next = Db::table('newsmc_xq')->where('id','>',input('id'))->order('id asc')->value('id');
        //取出上一篇和下一篇的标题
        $name_prev = Db::table('news_mc')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('news_mc')->where('id','>',input('id'))->order('id asc')->field('title')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);
        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);

        return $this->fetch('newsdetail');
    }
    //联系我们页面
    public function contact(){
        //导航栏栏目
        $column = Db::table('column')->select();
    	//Banner图
        $banner = Db::table('banner')->where('id=25')->find();

        $this->assign('column',$column);
        $this->assign('banner',$banner);

        return $this->fetch();
    }
    //联系我们留言
    public function add_xw(){
        //判断页面是否提交
        // print_r($_POST);exit();
        if(request()->isPost()){
            //接收传递过来的值
            //input都是封装好了，相当于POST['xxx'];
            $data = [
                'name' => input('name'),
                'address' => input('address'),
                'phone' => input('phone'),
                'content' => input('content'),
                'time' => time()
            ];
            // dump($data);exit;
            if(Db::table('message_mc')->insert($data)){  //添加数据
                return "<script>alert('留言成功');window.location.href='contact';</script>";
            }else{
                return "<script>window.location.href='contact';</script>";
            }
            return;
            
        }
        return $this->fetch('contact');
    }
}