<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

// Route::rule('index/:name', 'index/index');
// return [
//     '__pattern__' => [
//         'name' => '\w+',
//     ],
//     '[index]'     => [
//         ':id'   => ['index/index', ['method' => 'get'], ['id' => '\d+']],
//         ':name' => ['index/index', ['method' => 'post']],
//     ],

// ];
/*根目录*/
Route::rule('/','index/index');
/*视图*/
// Route::rule('course','index/course/about.html');
// Route::get('write','index/Write/write');
// Route::get('admin','admin/Admin/admin');
