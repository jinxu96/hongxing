<?php

namespace app\admin\controller;

use app\admin\common\Base;
use think\Db;
use think\Request;
use app\admin\model\Column as ColumnModel;

class Column extends Base
{	
	//展示页面
	public function index(){
		//分页数据
		$data = Db::table('column')->paginate(5);
		// dump($data);exit;
		$count = Db::table('column')->Count();
		//分配数据
		$this->assign('data',$data);
		$this->assign('count',$count);
		//渲染
		return $this->fetch('column_list');
	}

	//添加
	public function create(Request $request)
    {
        //1.设置返回的值
        $status = 1;
        $message = '添加成功';

        // 2.添加数据到分类表中
        $res = ColumnModel::create([
            'title'=> $request->param('title'),
            'title_name'=> $request->param('title_name'),
            'keywords'=> $request->param('keywords'),
            'description'=> $request->param('description')
        ]);

        //3.添加失败的处理
        if (is_null($res)) {
            $status = 0;
            $message = '添加失败';
        }

        return ['status'=> $status, 'message'=> $message, 'res'=> $res->toJson()];
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    //编辑
    public function edit(Request $request)
    {
        //1.获取id
        $id = $request -> param('id');

        //2.查询要更新的数据
        $data = ColumnModel::get($id);


        //4.模板赋值
        $this -> view -> assign('data', $data);


        //5.渲染模板
        return $this -> view -> fetch('column_edit');
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        //1.获取一下提交的数据
        $data = $request -> param();

        //2.更新操作
        $res = ColumnModel::update([
            'title' => $data['title'],
            'title_name' => $data['title_name'],
            'keywords' => $data['keywords'],
            'description' => $data['description']
        ],['id'=> $data['id']]);

        //3.设置返回值
        $status = 1;
        $message = '更新成功';

        //4.设置更新失败的返回值
        if (is_null($res)) {
            $status = 0;
            $message = '更新失败';
        }

        //5.返回结果
       return ['status'=>$status, 'message'=> $message];
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //删除
        ColumnModel::destroy($id);
    }
}