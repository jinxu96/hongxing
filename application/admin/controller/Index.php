<?php
namespace app\admin\controller;

use app\admin\common\Base;
use think\Db;

class Index extends Base
{
    public function index_one()
    {
        $this -> isLogin();
        return $this -> view -> fetch('index');
    }

    public function welcome()
    {	
    	$data = Db::table('admin')->select();
    	$date = date('Y-m-d H:i:s',$data[0]['last_time']);
    	$this->assign('date',$date);
        $this->assign('data',$data);
    	// dump($data);exit();
        return $this -> view -> fetch('welcome');
    }
}
