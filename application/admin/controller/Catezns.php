<?php

namespace app\admin\controller;

use app\admin\common\Base;
use think\Db;
use think\Request;
use app\admin\model\Catezns as CateznsModel;

class Catezns extends Base
{	
	//展示
	public function index(){
		//多表查询
        $cate = Db::table('catezns')
        	  ->alias('a')
        	  ->join('categoryzns b','b.pid=a.pid')
              ->select();
        // dump($cate);exit;
        $data = Db::table('catezns')->paginate(4);
        $count = Db::table('catezns')->Count();
        //分配数据
        $this->assign('cate',$cate);
        $this->assign('data',$data);
        $this->assign('count',$count);
        //渲染
        return $this->fetch('catezns_list');
	}
	//显示添加页面
	public function create()
    {
        $cate = Db::table('categoryzns')->select();
        // dump($cate);exit;

        $this->assign('cate',$cate);
        return $this->fetch('catezns_add');

    }
    //保存
	public function save()
    {
        //判断一下提交类型
        if ($this->request->isPost()) {

            //1.获取一下提交的数据,包括上传文件
            // $data = $this->request->param(true);
            
            //2获取一下上传的文件对象
            $file = $this->request->file('image');

            //3判断是否获取到了文件
            if (empty($file)) {
                $this->error($file->getError());
            }

            //4上传文件
            $map = [
                'ext'=>'jpg,png',
                'size'=> 3000000
            ];
            $info = $file->validate($map)->move(ROOT_PATH.'public/uploads/');
            if (is_null($info)){
                $this->error($file->getError());
            }

            //5向表中新增数据
            $data['title'] = input('title');
            $data['pid'] = input('pid');
            $data['content'] = input('content');
            $data['image'] = $info -> getSaveName();
            $data['time'] = time();

            $res = Db::table('catezns')->insert($data);

            //6判断新增是否成功
            if (is_null($res)){
                $this->error('新增失败');
            }

            $this->success('新增成功~~');

        }else {
            $this -> error('请求类型错误~~');
        }
    }
    //显示修改页面
    public function edit(){
        $id = input('id');
        $data = Db::table('catezns')
              ->alias('a')
              ->where("id",$id)
              ->join('categoryzns b','b.pid=a.pid')
              ->find();
         // dump($data);exit;
        $res = Db::table('categoryzns')->select();
        $this->assign('data',$data);
        $this->assign('res',$res);
        return $this->fetch('catezns_edit');
    }
    //修改资源
    public function update(Request $request)
    {
        // print_r($_POST);exit();
        //1.获取所有提交过来的数据，包括文件
       $data = $this ->request -> param(true);
       $data['time'] = time();
        //2.对于文件单独操作打包成一个文件对象
        $file = $this -> request -> file('image');

        //3.文件验证与上传
        $info = $file -> validate(['ext'=>'jpg,png','size'=>3000000])->move(ROOT_PATH.'public/uploads/');
        if (is_null($info)){
            $this->error($file->getError());
        }

        //4.执行更新操作
        $res = CateznsModel::update([
            'image'=> $info -> getSaveName(),
            'pid' => $data['pid'],
            'time' =>$data['time'],
            'content' => $data['content'],
            'title' => $data['title'],
        ],['id'=> $data['id']]);

        //5.检测更新
        if (is_null($res)) {
            $this -> error('更新失败~~');
        }

        //6.更新成功
        $this->success('更新成功~~');
    }
    //删除
    public function delete($id){
        //
        CateznsModel::destroy($id);
    }
}